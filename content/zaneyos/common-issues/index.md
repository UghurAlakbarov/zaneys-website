---
title: "Commonly Reported Issues"
description: "Even though ZaneyOS has had a ton of effort behind it issues creep up. So this is a list of common issues that users have reported and solutions or explainations of how to avoid them."
tags: ["Docs","NixOS","ZaneyOS"]
series: ["Documentation"]
series_order: 6
#externalUrl: ""
date: 2024-02-15
draft: false
authors:
  - Zaney
---

## No default/fallback boot loader

```
File system "/boot" is not a FAT EFI System Partition (ESP) file system.
systemd-boot not installed in ESP.
No default/fallback boot loader installed in ESP.
```

This error is caused by installing using MBR and not UEFI. It is important to use GPT and UEFI when installing the system. If you get this error just reinstall ensuring you are correcting booting and installing using UEFI.

## Hyprland Plugins Fail: (headers ver in not equal...

![Demo Of Plugin Headers Failing](img/pluginheadervermismatch.avif)

This can happen after updates. Don't freak out, you just need to reboot so the headers can be reloaded.

## Waybar does not load after a flake-rebuild

This issue can be caused by setting a waybar style value to an invalid value.

**Resolution:** Edit zaneyos/options.nix file, set the correct value and use the flake-rebuild alias.
